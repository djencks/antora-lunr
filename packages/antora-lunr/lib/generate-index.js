'use strict'

const lunr = require('lunr')
const cheerio = require('cheerio')
const entities = require('html-entities')
const zlib = require('zlib')

const INDEX_BASE = 'search-index'

module.exports.register = (eventEmitter, config = {}) => {
  config || (config = {})
  // console.log('config: ', config)
  var contentCatalog
  const indexer = {
    index: undefined,

    doGenerateIndex: (pages) => {
      this.index = generateIndexes(pages, contentCatalog, config)
    },

    doCreateIndexFile: async (siteFiles) => siteFiles.push(...(await createIndexFiles(this.index))),
  }

  eventEmitter.on('afterClassifyContent',
    (playbook, theContentCatalog) => {
      contentCatalog = theContentCatalog
    }
  )

  eventEmitter.on('afterConvertDocuments', (playbook, pages) => {
    indexer.doGenerateIndex(pages)
  })

  eventEmitter.on('afterMapSite', async (playbook, siteFiles) => {
    return indexer.doCreateIndexFile(siteFiles)
  })
}

function generateIndexes (pages, contentCatalog, config = {}) {
  return generateFilters(contentCatalog, config).map((filter) => generateIndex(pages, filter, config))
}

function generateFilters (contentCatalog, config) {
  // console.log('config: ', config)
  if (config.components) {
    var components = []
    if (config.components === '*') {
      components = contentCatalog.getComponents()
    } else if (Array.isArray(config.components)) {
      components = config.components.map((name) => contentCatalog.getComponent(name))
    } else {
      console.warn(`invalid components specification '${config.components}': allowed values are '*' or a list of component names`)
      return []
    }
    // console.log('components: ', components)
    return components.reduce((accum, component) => {
      if (config.versions) {
        if (config.versions === 'latest') {
          accum.push({ filter: latestFilter(component), indexName: `${component.name}_latest_${INDEX_BASE}` })
          return accum
        }
        if (config.versions === '*') {
          component.versions.map((version) => {
            accum.push({
              filter: filter(component, version),
              indexName: `${component.name}_${version.version}_${INDEX_BASE}`,
            })
          })
          return accum
        }
        console.warn(`invalid versions specification '${config.versions}': allowed values are 'latest' and '*'`)
        return accum
      }
      accum.push({ filter: (page) => page.src.component === component.name, indexName: `${component.name}_${INDEX_BASE}` })
      return accum
    }, [])
  }
  if (config.versions) {
    if (config.versions === 'latest') {
      return [{
        filter: (page) => page.src.version === contentCatalog.getComponent(page.src.component).latest.version,
        indexName: `latest_${INDEX_BASE}`,
      }]
    }
    console.warn(`invalid versions specification '${config.versions}': allowed value is 'latest'`)
    return []
  }
  return [{ filter: () => true, indexName: INDEX_BASE }]
}

function latestFilter (component) {
  return (page) => page.src.component === component.name && page.src.version === component.latest.version
}

function filter (component, version) {
  return (page) => page.src.component === component.name && page.src.version === version.version
}

/**
 * Generate a Lunr index.
 *
 * Iterates over the specified pages and creates a Lunr index.
 *
 * @memberof generate-index
 *
 * @param {Array<File>} pages - The publishable pages to map.
 * @returns {Object} A JSON object with a Lunr index and a documents store.
 */
function generateIndex (pages, filterInfo, config) {
  // console.log('filterInfo: ', filterInfo)
  if (!pages.length) return {}
  // Map of Lunr ref to document
  const documentsStore = {}
  const documents = pages
    .filter(filterInfo.filter)
    .map((page) => {
      if (page.asciidoc && page.asciidoc.attributes) {
        (page.asciidoc.attributes['page-lunr-indexname'] = filterInfo.indexName + '.js')
        // console.log('set page-lunr-indexname to ', filterInfo.indexName + '.js')
      }
      const html = page.contents.toString()
      const $ = cheerio.load(html)
      return { page, $ }
    })
    // Exclude pages marked as "noindex"
    .filter(({ page, $ }) => {
      return !(page.asciidoc && page.asciidoc.attributes && (page.asciidoc.attributes.noindex === '' || page.asciidoc.attributes.noindex))
    })
    .map(({ page, $ }) => {
      // we are indexing before page composing, so we only have the indexable content available.
      // Remove any found headings, to improve search results
      const article = $
      const $h1 = $('h1')
      const documentTitle = (page.asciidoc && page.asciidoc.attributes) ? page.asciidoc.attributes.doctitle : '(no title)'
      $h1.remove()
      const titles = []
      $('h2,h3,h4,h5,h6').each(function () {
        let $title = $(this)
        // If the title does not have an Id then Lunr will throw a TypeError
        // cannot read property 'text' of undefined.
        if ($title.attr('id')) {
          titles.push({
            text: $title.text(),
            id: $title.attr('id'),
          })
        }
        $title.remove()
      })

      //Remove configured excludes, thanks to Ewan Edwards!
      const exclude = config.exclude
      if (exclude) {
        exclude.forEach((excl) => {
          const $e = $(excl, article)
          $e.remove()
        })
      }

      // Pull the text from the article, and convert entities
      let text = article.text()
      // Decode HTML
      text = entities.decode(text, { level: 'html5' })
      // Strip HTML tags
      text = text.replace(/(<([^>]+)>)/ig, '')
        .replace(/\n/g, ' ')
        .replace(/\r/g, ' ')
        .replace(/\s+/g, ' ')
        .trim()

      // Return the indexable content, organized by type
      return {
        text: text,
        title: documentTitle,
        component: page.src.component,
        version: page.src.version,
        name: page.src.stem,
        url: page.pub.url,
        titles: titles, // TODO get title id to be able to use fragment identifier
      }
    })

  // Construct the lunr index from the composed content
  const lunrIndex = lunr(function () {
    const self = this
    self.ref('url')
    self.field('title', { boost: 10 })
    self.field('name')
    self.field('text')
    self.field('component')
    self.metadataWhitelist = ['position']
    documents.forEach(function (doc) {
      self.add(doc)
      doc.titles.forEach(function (title) {
        self.add({
          title: title.text,
          url: `${doc.url}#${title.id}`,
        })
      }, self)
    }, self)
  })

  // Place all indexed documents into the store
  documents.forEach(function (doc) {
    documentsStore[doc.url] = doc
  })

  // Return the completed index, store, and component map
  return {
    index: lunrIndex,
    store: documentsStore,
    indexName: filterInfo.indexName,
  }
}

// Helper function allowing Antora to create site assets containing the indexes
async function createIndexFiles (indexes) {
  return indexes.reduce(async (accum, index) => {
    const indexName = index.indexName
    delete index.indexName
    const contents = Buffer.from(`window.antoraLunr.init(${JSON.stringify(index)})`)
    accum.push({
      mediaType: 'text/javascript',
      contents,
      src: { stem: indexName },
      out: { path: `${indexName}.js` },
      pub: { url: `/${indexName}.js`, rootPath: '' },
    })
    accum.push({
      mediaType: 'text/javascript',
      contents: await new Promise((resolve, reject) => {
        zlib.gzip(contents, {}, (error, result) => {
          if (error) reject(Error(error))
          else resolve(result)
        })
      }),
      src: { stem: indexName },
      out: { path: `${indexName}.js.gz` },
      pub: { url: `/${indexName}.js.gz`, rootPath: '' },
    })
    return accum
  }, [])
}

module.exports._internal = { generateIndexes, createIndexFiles }
