'use strict'

/**
 * Lunr component for Antora
 *
 * @module lunr
 */
module.exports.register = require('./generate-index').register
