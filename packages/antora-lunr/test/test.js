/* eslint-env mocha */
const chai = require('chai')
const expect = chai.expect
const dirtyChai = require('dirty-chai')
chai.use(dirtyChai)
const { generateIndexes, createIndexFiles } = require('../lib/generate-index')._internal

describe('Generate index', () => {
  async function checkIndexFiles (indexes) {
    const files = await createIndexFiles(indexes)
    expect(files.length).to.equal(2)
  }

  it('should generate an empty index when there\'s no page', () => {
    // no page, no index!
    const pages = []
    const index = generateIndexes(pages)[0]
    expect(index).to.be.empty()
  })

  it('should generate an index', async () => {
    const pages = [{
      contents: Buffer.from('<article class="doc"><p>foo</p></article>'),
      src: {
        component: 'component-a',
        version: '2.0',
        stem: 'install-foo',
      },
      asciidoc: {},
      pub: {
        url: '/component-a/install-foo',
      },
    }]
    const indexes = generateIndexes(pages)
    const index = indexes[0]
    const installPage = index.store['/component-a/install-foo']
    expect(installPage.text).to.equal('foo')
    expect(installPage.component).to.equal('component-a')
    expect(installPage.version).to.equal('2.0')
    expect(index.index.search('foo'), 'foo is present in contents').to.have.lengthOf(1)
    expect(index.index.search('2.0'), '2.0 is not indexed').to.be.empty()
    expect(index.index.search('bar'), 'bar is not present').to.be.empty()
    expect(index.index.search('install-foo'), 'install-foo is present in url').to.have.lengthOf(1)
    expect(index.index.search('component-a'), 'component-a is present in component').to.have.lengthOf(1)
    expect(index.index.search('*foo*'), '*foo* is present in contents').to.have.lengthOf(1)
    expect(index.index.search('foo*'), 'foo* is present in contents').to.have.lengthOf(1)
    await checkIndexFiles(indexes)
  })

  it('should generate a document for each titles', async () => {
    const pages = [{
      contents: Buffer.from(`
<article class="doc">
  <p>The Static Site Generator for Tech Writers</p>
  <p>This site hosts the technical documentation for Antora</p>
  <h2 id="manage-docs-as-code">Manage docs as code</h2>
  <p>With Antora, you manage docs as code</p>
  <h3 id="where-to-begin">Where to begin</h3>
  <h4 id="navigation">Navigation</h4>
  <h5 id="link-types-syntax">Link Types & Syntax</h5>
  <h6 id="page-links">Page Links</h6>
</article>`),
      src: {
        component: 'hello',
        version: '1.0',
        stem: '',
      },
      asciidoc: { attributes: { doctitle: 'Antora Documentation' } },
      pub: {
        url: '/antora/1.0/',
      },
    }]
    const indexes = generateIndexes(pages)
    const index = indexes[0]
    const installPage = index.store['/antora/1.0/']
    expect(installPage.text).to.equal('The Static Site Generator for Tech Writers This site hosts the technical documentation for Antora With Antora, you manage docs as code')
    expect(installPage.component).to.equal('hello')
    expect(installPage.version).to.equal('1.0')
    expect(installPage.title).to.equal('Antora Documentation')
    expect(index.index.search('1.0'), 'version is not indexed').to.be.empty()
    expect(index.index.search('bar'), 'bar is not present').to.be.empty()
    expect(index.index.search('where to begin'), '"Where to begin" is indexed as a title').to.have.lengthOf(1)
    expect(index.index.search('docs as code'), '"docs as code" is indexed two times').to.have.lengthOf(2)
    expect(index.index.search('technical'), '"technical" is indexed').to.have.lengthOf(1)
    expect(index.index.search('hello'), '"hello" is indexed as component').to.have.lengthOf(1)
    await checkIndexFiles(indexes)
  })

  it('should only index the first document title (heading 1)', async () => {
    const pages = [{
      contents: Buffer.from(`
<article class="doc">
  <div id="preamble">
    <div class="sectionbody">
      <div class="paragraph">
        <p>Learn about what’s new in the 2.0 release series of Antora.</p>
      </div>
    </div>
  </div>
  <h1 id="antora-2-0-0" class="sect0"><a class="anchor" href="#antora-2-0-0"></a>Antora 2.0.0</h1>
  <div class="openblock partintro">
    <div class="content">
      <div class="paragraph">
        <p><em><strong>Release date:</strong> 2018.12.25 | <strong>Milestone (closed issues):</strong> <a href="https://gitlab.com/antora/antora/issues?milestone_title=v2.0.x&amp;scope=all&amp;state=closed" target="_blank" rel="noopener">v2.0.x</a></em></p>
      </div>
      <div class="paragraph">
        <p>The Antora 2.0.0 release streamlines the installation process, improves platform and library compatibility, provides a simpler and pluggable authentication mechanism for private repositories, and delivers the latest Asciidoctor capabilities.</p>
      </div>
    </div>
  </div>
</article>`),
      src: {
        component: 'hello',
        version: '1.0',
        stem: '',
      },
      asciidoc: { attributes: { doctitle: 'What’s New in Antora' } },
      pub: {
        url: '/antora/1.0/whats-new.html',
      },
    }]
    const indexes = generateIndexes(pages)
    const index = indexes[0]
    const whatsNewPage = index.store['/antora/1.0/whats-new.html']
    expect(whatsNewPage.title).to.equal('What’s New in Antora')
    await checkIndexFiles(indexes)
  })

  it('should exclude pages with noindex defined as attribute', async () => {
    const pages = [{
      contents: Buffer.from(`
<html lang="en">
  <body class="article">
    <main role="main">
      <article class="doc">
        <h1 class="page">Antora Documentation</h1>
        <div class="sect1">
          <h2 id="manage-docs-as-code"><a class="anchor" href="#manage-docs-as-code"></a>Manage docs as code</h2>
          <div class="sectionbody">
            <div class="paragraph">
              <p>With Antora, you manage <strong>docs as code</strong>.
              That means your documentation process benefits from the same practices used to produce successful software.</p>
            </div>
          </div>  
        </div>
      </article>
    </main>
  </body>  
</html>`),
      src: {
        component: 'hello',
        version: '1.0',
        stem: '',
      },
      asciidoc: {
        attributes: {
          noindex: '',
        },
      },
      pub: {
        url: '/antora/1.0/',
      },
    },
    {
      contents: Buffer.from(`
<html lang="en">
<body class="article">
  <main role="main">
    <article class="doc">
      <h1 class="page">How Antora Can Help You and Your Team</h1>
      <div class="sect1">
        <h2 id="agile-and-secure"><a class="anchor" href="#agile-and-secure"></a>Agile and secure</h2>
        <div class="sectionbody">
          <div class="paragraph">
              <p><strong>Automate the assembly of your secure, nimble static site as changes happen instead of wrestling with a CMS giant.</strong></p>
          </div>
        </div>
      </div>
    </article>
  </main>
</body>  
</html>`),
      src: {
        component: 'hello',
        version: '1.0',
        stem: '',
      },
      asciidoc: {},
      pub: {
        url: '/antora/1.0/features/',
      },
    }]
    const indexes = generateIndexes(pages)
    const index = indexes[0]
    const privatePage = index.store['/antora/1.0/']
    expect(privatePage).to.be.undefined()
    const featuresPage = index.store['/antora/1.0/features/']
    expect(featuresPage.text).to.equal('Automate the assembly of your secure, nimble static site as changes happen instead of wrestling with a CMS giant.')
    await checkIndexFiles(indexes)
  })

  describe('Paths', () => {
    it('should use relative links when site URL is not defined', () => {
      const pages = [{
        contents: Buffer.from('foo'),
        src: {
          component: 'component-a',
          version: '2.0',
          stem: 'install-foo',
        },
        pub: {
          url: '/component-a/install-foo',
        },
      }]
      const index = generateIndexes(pages)[0]
      expect(index.store['/component-a/install-foo'].url).to.equal('/component-a/install-foo')
    })

    it('should use relative links when site URL is a relative path', () => {
      const pages = [{
        contents: Buffer.from('foo'),
        src: {
          component: 'component-a',
          version: '2.0',
          stem: 'install-foo',
        },
        pub: {
          url: '/component-a/install-foo',
        },
      }]
      const index = generateIndexes(pages)[0]
      expect(index.store['/component-a/install-foo'].url).to.equal('/component-a/install-foo')
    })

    it('should use relative links when site URL is an absolute local path (using file:// protocol)', () => {
      const pages = [{
        contents: Buffer.from('foo'),
        src: {
          component: 'component-a',
          version: '2.0',
          stem: 'install-foo',
        },
        pub: {
          url: '/component-a/install-foo',
        },
      }]
      const index = generateIndexes(pages)[0]
      expect(index.store['/component-a/install-foo'].url).to.equal('/component-a/install-foo')
    })
  })
})
